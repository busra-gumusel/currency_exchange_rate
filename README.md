### How to run this project?

Run this command under the root of project: docker-compose up --build

Test it out:  http://0.0.0.0:8000

### Run tests: 

docker-compose exec web python manage.py test

